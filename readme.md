# Readme

A small setup for a pubnix/tilde-like user overview website.

## Concept

Run this with [caddy](https://caddyserver.com/).

The main handle `/*` will run in the context of the web folder and serve the index.html and static files.
The `index.html` is a template, which checks if users have a `public/index.html` file in their home folder, and if so, generate a link to `~/username` - which is served by the `@user_home` handle.

start with `caddy run --config Caddyfile`

